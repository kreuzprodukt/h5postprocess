#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  h5posty.py
#
#  Script for post-processing hdf5 carpet files.
#  Reads 3 hdf3 carpet files in read mode, 1 in write mode.
#  Compares values stored in compose h5 output file and writes values.
#
#  Copyright 2018 Michael T. Rattay <yoon@gmx.de>

import h5py
import numpy as np

def calc_quark(rho, temp, ye):
    x0 = 0
    y0 = 0 
    z0 = 0
    while eos_rho[x0]/0.15 < aurho/normalnuc*rho:
      x0 = x0 + 1
    while eos_temp[y0] < temp:
      y0 = y0 + 1
    while eos_y_e[z0] < ye:
      z0 = z0 + 1
    #print str(aurho/normalnuc*rho) + " < " + str(eos_rho[x0]/0.15)
    #print str(temp) + " < " + str(eos_temp[y0])
    #print str(ye) + " < " + str(eos_y_e[z0])
    #return [x0,y0,z0]
    return eos_y_q[x0*n_eos_temp*n_eos_y_e + y0*n_eos_y_e + z0]

#units
timeperit = 16.8/320
uc = 2.99792458*10**(10)
uG = 6.67428*10**(-8)
uMs = 1.9884*10**(33)
utime = uG*uMs/uc**3*1000
ulenght = (uG*uMs/uc**2)/100000
urho = (uc**6)/(uG**3*uMs**2)
uangmomentum = uG*uMs**2/uc
normalnuc = 2.705*10**(14)

auc = 2.99792458*10**(10)
auG = 6.67428*10**(-8)
auMs = 1.9884*10**(33)
aurho = (auc**6)/(auG**3*auMs**2)
normalnuc = 2.705*10**(14)

print "Opening data sets ..."
#Load hdf5 files
#r for read only
#a for read/write
h5_rho = h5py.File('rho_b.xyz.h5', 'r')
h5_temp = h5py.File('temp.xyz.h5', 'r')
h5_y_e = h5py.File('ye.xyz.h5', 'r')
h5_target = h5py.File('quark.xyz.h5', 'a')
h5_target_len = len(h5_target.keys())
h5_list = [h5_rho.keys(), h5_temp.keys(), h5_y_e.keys(), h5_target.keys()]

h5_compose = h5py.File('eoscompose.h5', 'r')

#Load EOS compose files
eos_rho = np.array(h5_compose['nb'])
eos_temp = np.array(h5_compose['t'])
eos_y_e = np.array(h5_compose['yq'])
eos_y_q = np.array(h5_compose['yi'])

#print eos_rho

#Load length of parameter lists
n_eos_rho = h5_compose['pointsnb']
n_eos_rho = n_eos_rho[0]
n_eos_y_e = h5_compose['pointsyq']
n_eos_y_e = n_eos_y_e[0]
n_eos_temp = h5_compose['pointst']
n_eos_temp = n_eos_temp[0]

print "Number of density points = " + str(n_eos_rho)
print "Number of temperature points = " + str(n_eos_temp)
print "Number of charge fraction points = " + str(n_eos_y_e)

#conversion of density units
#for i in range (0, n_eos_rho):
#  print "Value in fm⁻³/rho_nuclear  = " + str(eos_rho[i]/0.15)
#  eos_rho[i]=normalnuc/(aurho*eos_rho[i])
#  print "Converted to = " + str(eos_rho[i])

#print 0.0012247188985296822 * aurho/normalnuc

#loop over all iterations and refinement levels, (-1) to ignore parameter dataset
for it in range (0, h5_target_len-1):
    print "Processing: " + str(h5_rho[h5_list[0][it]])
    data_target = h5_target[h5_list[3][it]]
    data_rho = h5_rho[h5_list[0][it]]
    data_temp = h5_temp[h5_list[1][it]]
    data_y_e = h5_y_e[h5_list[2][it]]
    for x in range(0, data_target.shape[0]):
      for y in range(0, data_target.shape[1]):
        for z in range(0, data_target.shape[2]):
          #data_target[x,y,z]= 0.1
          #print calc_quark(data_rho[x,y,z], data_temp[x,y,z], data_y_e[x,y,z])
          data_target[x,y,z] = calc_quark(data_rho[x,y,z], data_temp[x,y,z], data_y_e[x,y,z])